from dotenv import load_dotenv
import socket
import os


load_dotenv('.env')


if __name__ == "__main__":
    addr = os.environ.get('SERVER_ADDR')
    port = int(os.environ.get('SERVER_PORT'))
    print(port, addr)
    if len(os.sys.argv) == 1:
        print('Volume value parameter wasnt found')
        exit()
    sock = socket.socket()
    sock.connect((addr, port))
    sock.send(os.sys.argv[1].encode())
    sock.close()
