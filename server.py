from dotenv import load_dotenv
from sound import Sound
import os
import socket


load_dotenv('.env')


if __name__ == "__main__":
    sock = socket.socket()
    serv_addr = os.environ.get('SERVER_ADDR')
    serv_port = int(os.environ.get('SERVER_PORT'))
    sock.bind((serv_addr, serv_port))
    sock.listen(1)
    while True:
        conn, addr = sock.accept()
        print(addr)
        data = conn.recv(1024)
        if not data:
            conn.close()
            continue
        data = int(data.decode())
        if data == -1:
            break
        Sound.volume_set(int(data))
    sock.close()

